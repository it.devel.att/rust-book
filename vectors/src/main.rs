use rand::Rng;
use std::collections::HashMap;

fn main() {
    let mut rng = rand::thread_rng();
    let mut nums: Vec<u32> = Vec::new();

    let rnd_len: u32 = rng.gen_range(50..100);
    for _ in 0..rnd_len {
        let x: u32 = rng.gen_range(0..100);
        nums.push(x);
    }
    println!("{:?}", nums);

    let sum: u32 = nums.iter().sum();
    let num_len = nums.len();
    let average = sum as f32 / num_len as f32;
    let middle = nums.len() / 2;
    let middle_num = nums[middle];

    println!("Average is {}", average);
    println!("middle is {}, Fullength is {}", middle, num_len);
    println!("Middle num is {}", middle_num);

    let mut count_nums: HashMap<u32, u32> = HashMap::new();
    for i in nums {
        *count_nums.entry(i).or_insert(0) += 1;
    }
    println!("{:?}", count_nums);
    let mut often_number: (u32, u32) = (0, 0);
    for (key, value) in count_nums {
        if value > often_number.1 {
            often_number.0 = key;
            often_number.1 = value;
        }
    }
    println!(
        "More often number is {}. Times {}",
        often_number.0, often_number.1
    );

    // Pig Latin
    let vowels: Vec<char> = vec!['a', 'e', 'i', 'o', 'u', 'y'];
    let vowels_ending = "-hay";
    let consonants_ending = "ay";

    let some_word = String::from("first");
    let first_letter = some_word.chars().nth(0).unwrap_or_default();
    if vowels.contains(&first_letter) {
        println!("{}{}", some_word, vowels_ending);
    } else {
        let pig_consonants_word = String::from(some_word);
        let rest_work: String = pig_consonants_word[1..].to_string();
        println!("{}-{}{}", rest_work, first_letter, consonants_ending);
    }
}
