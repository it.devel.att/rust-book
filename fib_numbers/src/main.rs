use std::io;
use num_bigint::BigUint;
use num_bigint::{ToBigUint};

fn main() {
    println!("Введите число");

    let mut number = String::new();
    io::stdin()
        .read_line(&mut number)
        .expect("Ошибка при чтении строки");

    let number: i64 = number.trim().parse().expect("Введено не число");
    let mut fibb_numbers: Vec<BigUint> = Vec::new();
    for i in 0..number + 1 {
        if i != 0 && i != 1 {
            let index: usize = i as usize;
            let next_number: BigUint = fibb_numbers[index - 1].clone() + fibb_numbers[index - 2].clone(); 
            fibb_numbers.push(next_number);
            continue;
        }
        fibb_numbers.push(i.to_biguint().unwrap())
    }
    println!("Последовательность фибоначи для числа {}", number);
    let fibb_numbers: Vec<String> = fibb_numbers.into_iter().map(|value| value.to_string()).collect();
    println!("{}", fibb_numbers.join(", "))
}
