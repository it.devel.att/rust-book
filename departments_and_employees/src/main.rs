// use std::collections::HashMap;
// use std::io;

fn some_func(val: &str) {
    println!("{}", val);
}

fn some_other_func(val: String) {
    println!("{}", val);
}

fn main() {
    let my_var: &str = "hello";
    some_func(&my_var);
    println!("{}", my_var); // Is valid here;

    let other_var: String = String::from("hello_2");
    some_other_func(other_var);

    const MY_CONST: &str = "awesome";
    some_func(MY_CONST);
    println!("{}", MY_CONST);

    const ANOTHER_CONST: &str = "awesome_CONST";

    // println!("{}", other_var); // Not valid here
}
    // let mut departments_and_employees: HashMap<String, Vec<String>> = HashMap::new();
    // loop {
    //     let add_to_department_pattern = "Pattern is: \"Add %employee_name% to %department%!\"";
    //     let show_department_employees = "Parrern is: \"Show department %department_name%\"";
    //     println!("Enter the command. {}", add_to_department_pattern);
    //     println!("Print q for quit");

    //     let mut user_command = String::new();
    //     io::stdin().read_line(&mut user_command).expect("Error!");
        
    //     let commands: Vec<&str> = user_command.trim().split(" ").collect();
    //     let action = commands[0];
        
    //     match action {
    //         "add" => {
    //             if commands.len() != 4 {
    //                 println!("Wrong command! {}", add_to_department_pattern);
    //                 continue;
    //             }
    //             let employee_name = commands[1];
    //             let department = commands[3];

    //             departments_and_employees
    //                 .entry(department.to_string())
    //                 .or_insert(vec![])
    //                 .push(employee_name.to_string());
    //         }
    //         "show" => {
    //             if commands.len() < 2 {
    //                 println!("Wrong command! {}", show_department_employees);
    //                 continue;
    //             }
    //             let subcommand = commands[1];
    //             match subcommand {
    //                 "department" => {
    //                     if commands.len() < 3 {
    //                         println!("Wrong command! Third value must be a department name!");
    //                         continue;
    //                     }
    //                     let department = commands[2];

    //                     let department_with_employees = departments_and_employees.get(department);
    //                     match department_with_employees {
    //                         Some(employees) => {
    //                             println!("Employees of {} department:", department);
    //                             for (i, employee) in employees.iter().enumerate() {
    //                                 println!("{}: {}", i + 1, employee);
    //                             }
    //                             println!();
    //                         }
    //                         None => {
    //                             println!("Department \"{}\" not exists", department);
    //                             continue;
    //                         }
    //                     }
    //                 }
    //                 "employees" => {
    //                     let mut all_employees: Vec<String> = vec![];
    //                     for (_, value) in departments_and_employees.iter() {
    //                         all_employees.extend(value.clone());
    //                     }
    //                     all_employees.sort();

    //                     for (i, employee) in all_employees.iter().enumerate() {
    //                         println!("{}: {}", i, employee)
    //                     }
    //                 },
    //                 _ => {
    //                     println!("Unknown subcommand {}", subcommand);
    //                 }
    //             }
    //         }
    //         "q" => {
    //             println!("Goodbye!");
    //             break;
    //         }
    //         _ => {
    //             println!("Unknow command: {}", action);
    //             continue;
    //         }
    //     }
    // }
// }
