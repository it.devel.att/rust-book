use std::io;
use std::collections::HashMap;

fn factorial_with_counter(number: i32, count: i32, hash_map: &mut HashMap<i32, i32>) -> i32 {
    if number == 1 || number == 0 {
        return number
    }
    let result = number * factorial_with_counter(number - 1, count - 1, hash_map);
    hash_map.insert(count, result);
    result
}

fn main() {
    println!("Факториалы!");

    println!("Введите число");
    let mut number = String::new();
    let mut number_and_fib_result: HashMap<i32, i32> = HashMap::new();
    
    io::stdin()
        .read_line(&mut number)
        .expect("Ошибка при чтении ввода");

    let number = number.trim().parse().expect("Ввели не строку!");
    let fibb_result = factorial_with_counter(number, number, &mut number_and_fib_result);
    println!("Результат: {}", fibb_result);
    let mut sorted: Vec<_> = number_and_fib_result.iter().collect();
    sorted.sort_by_key(|a| a.0);

    for (count, number) in sorted.iter() {
        println!("Число {}, факториал {}", count, number);
    }
}
