fn longest<'a, 'b>(str1: &'a str, str2: &'b str) -> &'a str {
    // if str1.len() > str2.len() {
        // str1
    // } else {
        // str2
    // }
    println!("str1 = {}", str1);
    println!("str2 = {}", str2);
    // str2;
    str1
}

trait Summary {
    fn summarize(&self) -> String;
}


struct Post {
    title: String,
    description: String,
}

impl Summary for Post {
    fn summarize(&self) -> String {
        format!("Title: {}\nDescription: {}", self.title, self.description)
    }
}

fn notify(item: &impl Summary) {
    println!("{}", item.summarize())
}


fn main() {
    // First example
    let string1 = String::from("abcs");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);

    // Second
    let str1 = String::from("hello world");
    let result;
    {
        let str2 = String::from("xyz");
        result = longest(str1.as_str(), str2.as_str());
        println!("The longest string is {}", result);
        println!("str2 {}", str2);
    }

    let post = Post{
        title: String::from("Some title"),
        description: String::from("Some description"),
    };
    notify(&post);
}
