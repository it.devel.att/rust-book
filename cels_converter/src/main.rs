use std::io;

fn main() {
    println!("Введите температуру в Фаренгейтах");

    loop {
        let mut far_temp = String::new();
        io::stdin()
        .read_line(&mut far_temp)
        .expect("Failed to read line");

        if far_temp.trim() == "q" {
            println!("Досвиданья!");
            break;
        }
    
        let far_temp: i32 = match far_temp.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("You are enter not a number");
                continue;
            }
        };
        let cels_temp: f64 = (far_temp - 32) as f64 / 1.800;
        println!("Температура в цельскиях: {}", cels_temp)
    }

}