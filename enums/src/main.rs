
#[derive(Debug)]
enum UsState {
    Alabama,
    Alaska,
}

#[derive(Debug)]
enum Coin {
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
    match coin {
        Coin::Penny => {
            println!("Lucky penny!");
            1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}", state);
            25
        },
    }
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    match x {
        Some(i) => Some(i + 1),
        None => None,
    }
}

fn some_values(x: Option<i32>) -> Option<i32> {
    match x {
        Some(1) => Some(1 + 1),
        None => None,
        _ => Some(11 + 11),
    }
}

fn main() {
    let coin = Coin::Penny;
    let alabama_quarter = Coin::Quarter(UsState::Alaska);
    value_in_cents(coin);
    value_in_cents(alabama_quarter);

    let five = Some(5);
    let six = plus_one(five);
    let none = plus_one(None);
    println!("six {:?}", six);
    println!("none {:?}", none);

    let two = Some(2);
    let one = Some(1);
    println!("{:?}", some_values(two));
    println!("{:?}", some_values(one));

}
