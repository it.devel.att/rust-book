use add_one;
use rand;

fn main() {
    let num = 10;
    println!("{}", add_one::add_one(num));
}
